package edu.missouristate.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdb2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootdb2Application.class, args);
	}

}
