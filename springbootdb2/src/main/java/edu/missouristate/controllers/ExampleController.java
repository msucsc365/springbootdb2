package edu.missouristate.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import edu.missouristate.model.Example;
import edu.missouristate.services.ExampleService;

@Controller
public class ExampleController {

	@Autowired
	ExampleService exampleService;
	
	@GetMapping("/example/table")
	public String getExampleTable(Model model, HttpServletRequest request, HttpSession session) {
		List<Example> exampleList = exampleService.getExamples();
		model.addAttribute("exampleList", exampleList);
		return "exampleTable";
	}
	
	
}
